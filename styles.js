(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["styles"],{

/***/ "+EN/":
/*!*************************!*\
  !*** ./src/styles.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "LboF");
            var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--13-1!../node_modules/postcss-loader/src??embedded!../node_modules/resolve-url-loader??ref--13-3!../node_modules/sass-loader/dist/cjs.js??ref--13-4!./styles.scss */ "/I9Y");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "/I9Y":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--13-1!./node_modules/postcss-loader/src??embedded!./node_modules/resolve-url-loader??ref--13-3!./node_modules/sass-loader/dist/cjs.js??ref--13-4!./src/styles.scss ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "JPst");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "fieldset {\n  padding: 0.35em 0.625em 0.75em;\n  margin: 0 2px;\n  border: 1px solid silver;\n}\nfieldset legend {\n  width: unset;\n}\n.table-fixed {\n  width: 100%;\n  background-color: #f3f3f3;\n}\n.table-fixed tbody {\n  overflow-y: auto;\n  width: 100%;\n}\n.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {\n  display: block;\n}\n.table-fixed tbody td {\n  float: left;\n  height: 4em;\n}\n.table-fixed thead tr th {\n  float: left;\n  border-color: #e67e22;\n}\n.table-scroll {\n  /*width:100%; */\n  display: block;\n  empty-cells: show;\n  /* Decoration */\n  border-spacing: 0;\n  border: 1px solid;\n}\n.table-scroll thead {\n  background-color: #f1f1f1;\n  position: relative;\n  display: block;\n  width: 100%;\n  overflow-y: scroll;\n}\n.table-scroll tbody {\n  /* Position */\n  display: block;\n  position: relative;\n  width: 100%;\n  overflow-y: scroll;\n  /* Decoration */\n  border-top: 1px solid rgba(0, 0, 0, 0.2);\n}\n.table-scroll tr {\n  width: 100%;\n  display: flex;\n}\n.table-scroll td, .table-scroll th {\n  flex-basis: 100%;\n  flex-grow: 2;\n  display: block;\n  padding: 1rem;\n  text-align: left;\n  min-width: 50px;\n}\n.onoffswitch-inner[data-label-on]:before {\n  content: attr(data-label-on);\n}\n.onoffswitch-inner[data-label-off]:after {\n  content: attr(data-label-off);\n}\n/*table*/\n.table-responsive {\n  overflow: auto;\n}\n.table-responsive .table-container {\n  position: relative;\n  width: -webkit-max-content;\n  width: max-content;\n}\n.table-responsive .table-head {\n  width: -webkit-max-content;\n  width: max-content;\n  position: sticky;\n  top: 0;\n  background: #fff;\n}\n.table-responsive .table-head > div, .table-responsive .table-body thead tr:only-child {\n  height: 30px;\n  line-height: 30px;\n}\n.table-responsive .table-head > div {\n  border-width: 1px;\n  font-weight: 700;\n  display: inline-block;\n  text-align: center;\n  top: -20px;\n}\n.table-responsive .table-body {\n  overflow: auto;\n  margin-top: -30px;\n}\n.table-responsive .table-head > div, .table-responsive .table-container th, .table-responsive .table-container td {\n  border-right: 1px solid #e0e0e0;\n  min-width: 100px;\n  overflow: hidden;\n}\n.table-responsive .table-head > div input, .table-responsive .table-container th input, .table-responsive .table-container td input {\n  width: 95%;\n  border-width: 0px;\n}\n/* Other options */\n.dashboard-box {\n  padding: 30px;\n}\n.table-scroll.small-first-col td:first-child,\n.table-scroll.small-first-col th:first-child {\n  flex-basis: 20%;\n  flex-grow: 1;\n}\n.table-scroll tbody tr:nth-child(2n) {\n  background-color: rgba(130, 130, 170, 0.1);\n}\n.body-half-screen {\n  max-height: 50vh;\n}\n.small-col {\n  flex-basis: 10%;\n}\n.pl-25px {\n  padding-left: 25px;\n}\n.page-content-wrapper .page-content {\n  min-height: calc(100vh - 46px);\n}\n.cursor-pointer {\n  cursor: pointer;\n}\n.active td {\n  color: red;\n}\n.modal-open .fadeInRight {\n  animation: unset;\n}", "",{"version":3,"sources":["webpack://src/styles.scss"],"names":[],"mappings":"AAAA;EACI,8BAAA;EACA,aAAA;EACA,wBAAA;AACJ;AAAI;EACI,YAAA;AAER;AACA;EACI,WAAA;EACA,yBAAA;AAEJ;AADI;EACI,gBAAA;EACA,WAAA;AAGR;AADI;EACI,cAAA;AAGR;AAAQ;EACI,WAAA;EACA,WAAA;AAEZ;AAGY;EACI,WAAA;EAEA,qBAAA;AAFhB;AAOA;EACI,eAAA;EACA,cAAA;EACA,iBAAA;EAEA,eAAA;EACA,iBAAA;EACA,iBAAA;AALJ;AAQA;EACI,yBAAA;EACA,kBAAA;EACA,cAAA;EACA,WAAA;EACA,kBAAA;AALJ;AAQA;EACI,aAAA;EACA,cAAA;EAAgB,kBAAA;EAChB,WAAA;EAAY,kBAAA;EACZ,eAAA;EACA,wCAAA;AAHJ;AAMA;EACI,WAAA;EACA,aAAA;AAHJ;AAMA;EACI,gBAAA;EACA,YAAA;EACA,cAAA;EACA,aAAA;EACA,gBAAA;EACA,eAAA;AAHJ;AAKA;EACI,4BAAA;AAFJ;AAIA;EACI,6BAAA;AADJ;AAIA,QAAA;AAEA;EACI,cAAA;AAFJ;AAGI;EAAkB,kBAAA;EAAmB,0BAAA;EAAA,kBAAA;AACzC;AAAI;EACI,0BAAA;EAAA,kBAAA;EACA,gBAAA;EACA,MAAA;EACA,gBAAA;AAER;AAAI;EACI,YAXa;EAYb,iBAZa;AAcrB;AAAI;EAAmB,iBAAA;EAAmB,gBAAA;EAAkB,qBAAA;EAAuB,kBAAA;EAAoB,UAAA;AAOvG;AANI;EAAe,cAAA;EAAgB,iBAAA;AAUnC;AATI;EACI,+BAAA;EACA,gBAAA;EACA,gBAAA;AAWR;AAVQ;EACI,UAAA;EACA,iBAAA;AAYZ;AAPA,kBAAA;AAEA;EACI,aAAA;AASJ;AAPA;;EAEI,eAAA;EACA,YAAA;AAUJ;AAPA;EACI,0CAAA;AAUJ;AAPA;EACI,gBAAA;AAUJ;AAPA;EAAW,eAAA;AAWX;AATA;EACI,kBAAA;AAYJ;AATI;EACI,8BAAA;AAYR;AATA;EACI,eAAA;AAYJ;AAVA;EACI,UAAA;AAaJ;AAVI;EAAa,gBAAA;AAcjB","sourcesContent":["fieldset {\r\n    padding: .35em .625em .75em;\r\n    margin: 0 2px;\r\n    border: 1px solid silver;\r\n    legend {\r\n        width: unset;\r\n    }\r\n}\r\n.table-fixed{\r\n    width: 100%;\r\n    background-color: #f3f3f3;\r\n    tbody{\r\n        overflow-y:auto;\r\n        width: 100%;\r\n    }\r\n    thead,tbody,tr,td,th{\r\n        display:block;\r\n    }\r\n    tbody{\r\n        td{\r\n            float:left;\r\n            height: 4em;\r\n        }\r\n    }\r\n    thead {\r\n        tr{\r\n            th{\r\n                float:left;\r\n                //background-color: #f39c12;\r\n                border-color:#e67e22;\r\n            }\r\n        }\r\n    }\r\n}\r\n.table-scroll{\r\n    /*width:100%; */\r\n    display: block;\r\n    empty-cells: show;\r\n\r\n    /* Decoration */\r\n    border-spacing: 0;\r\n    border: 1px solid;\r\n}\r\n\r\n.table-scroll thead{\r\n    background-color: #f1f1f1;\r\n    position:relative;\r\n    display: block;\r\n    width:100%;\r\n    overflow-y: scroll;\r\n}\r\n\r\n.table-scroll tbody{\r\n    /* Position */\r\n    display: block; position:relative;\r\n    width:100%; overflow-y:scroll;\r\n    /* Decoration */\r\n    border-top: 1px solid rgba(0,0,0,0.2);\r\n}\r\n\r\n.table-scroll tr{\r\n    width: 100%;\r\n    display:flex;\r\n}\r\n\r\n.table-scroll td,.table-scroll th{\r\n    flex-basis:100%;\r\n    flex-grow:2;\r\n    display: block;\r\n    padding: 1rem;\r\n    text-align:left;\r\n    min-width: 50px;\r\n}\r\n.onoffswitch-inner[data-label-on]:before {\r\n    content: attr(data-label-on);\r\n}\r\n.onoffswitch-inner[data-label-off]:after {\r\n    content: attr(data-label-off);\r\n}\r\n\r\n/*table*/\r\n$height-table-head : 30px;\r\n.table-responsive {\r\n    overflow: auto;\r\n    .table-container {position: relative;width: max-content}\r\n    .table-head {\r\n        width: max-content;\r\n        position:sticky;\r\n        top: 0;\r\n        background: #fff;\r\n    }\r\n    .table-head> div, .table-body thead tr:only-child {\r\n        height: $height-table-head;\r\n        line-height: $height-table-head;\r\n    }\r\n    .table-head> div { border-width: 1px; font-weight: 700; display: inline-block; text-align: center; top: -20px;}\r\n    .table-body {  overflow: auto; margin-top: -$height-table-head}\r\n    .table-head> div, .table-container th, .table-container td {\r\n        border-right: 1px solid #e0e0e0;\r\n        min-width: 100px;\r\n        overflow: hidden;\r\n        input {\r\n            width: 95%;\r\n            border-width: 0px;\r\n        }\r\n    }\r\n}\r\n\r\n/* Other options */\r\n\r\n.dashboard-box {\r\n    padding: 30px;\r\n}\r\n.table-scroll.small-first-col td:first-child,\r\n.table-scroll.small-first-col th:first-child{\r\n    flex-basis:20%;\r\n    flex-grow:1;\r\n}\r\n\r\n.table-scroll tbody tr:nth-child(2n){\r\n    background-color: rgba(130,130,170,0.1);\r\n}\r\n\r\n.body-half-screen{\r\n    max-height: 50vh;\r\n}\r\n\r\n.small-col{flex-basis:10%;}\r\n\r\n.pl-25px {\r\n    padding-left: 25px;\r\n}\r\n.page-content-wrapper {\r\n    .page-content {\r\n        min-height: calc(100vh - 46px);\r\n    }\r\n}\r\n.cursor-pointer{\r\n    cursor: pointer;\r\n}\r\n.active td{\r\n    color: red;\r\n}\r\n.modal-open {\r\n    .fadeInRight{animation: unset}\r\n}\r\n"],"sourceRoot":""}]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 2:
/*!*******************************!*\
  !*** multi ./src/styles.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Project\Labix_compound\src\styles.scss */"+EN/");


/***/ }),

/***/ "JPst":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "LboF":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && btoa) {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ })

},[[2,"runtime"]]]);
//# sourceMappingURL=styles.js.map